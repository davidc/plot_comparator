# Plot comparator
Tons of plots? This online tool allows you to compare them two by two while (ab)using the clicks on radio buttons. See an example <a href="https://cern.ch/davidc/plots/comparator_tthbb_isovars/" target="_blank">here</a>. The tool displays plots in _png_ format, clicking on the image gets you the _pdf_ version.

## How to get it
You need a web space ```~/www``` or ```~/public_html``` where you clone/fork this repository. Your plots should also be in a sub-directory from there.

### One command
The HTML index page is generated with the command:
```
python3 make_plot_comparator.py config/YOUR_JSON_FILE.json
```
<sub>Ok two commands if you do not have python3 by default, then ```source setup.sh``` and you'll get it.</sub>

What does this script do? It will create a directory (within your ```~/www``` space) with an ```index.html``` and all the subfolders ```css/```, ```/js``` and ```img/``` to make the whole thing work.

Let's go through an example to show you how to best write your own json configuration file. 

### Configuration
Have a look at the file <a href="https://gitlab.cern.ch/davidc/plot_comparator/tree/master/config/example_config_ttHbb_study.json" target="_blank">config/example_config_ttHbb_study.json</a> to see the tags:
   - **output_directory**: the to-be-created folder with the index.html of the generated plot comparator.
   - **plots_directory**: the folder where your plots are located (they stay there)
   - **title**: title of the web page.
   - **subtitle**: description of the study, more info, etc. 
   - **placeholder**: link to the image to display if the plot does not exist. See [here](./img) some possibilities.
   - **tags**: dictionary of dictionares storing all the options of your plots. More on this below.

The tags are part of the plot file names. This plot comparator uses a double undescore ```__``` to separate tags. 
My scheme is: the year (20152016, 2017, 2018), the lepton type ("el" or "mu"), its selection ("tight" or "loose"), the number of jets (4ij for inclusive), the number of b-jets, the range of lepton pT and an extra cut, if any.
Here is a non-exhaustive list:
```
2017__el__tight__HT_jets__4ij__2ib__pTlepFull__none.png
2018__mu__tight__met_met__4ij__2ib__pTlepFull__none.png
2018__el__tight__nJets__4ji__3eb__pTlep30-40__HT_jets_above_200.png
```
The keys in the ```tags``` dictionary are the tag name. The values are:
   - **active**: boolean true/false. There may be a sub-string in all your plot file names always the same: create for that an non-active tag so that the plot comparator takes the value to build the complete file name.
   - **htmlLabel**: what is displayed in the HTML file in bold before the list of radio buttons
   - **values**: a dictonary listing the radio button names. The keys here correspond to the plot's tag string in the plot file name (e.g. "20152016" or "el_pt") and the value is the HTML label of the radio button name (e.g. "2015 - 2016" or "Lepton p<sub>T</sub>") displayed on the resulting web page.

You may have to adapt this with the way your plots are named. Or rename your plots accordingly if it is simpler for you.
Happy forking, tweaking and clicking!

