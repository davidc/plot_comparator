
window.onload=function() 
{
    /*alert(data.title);*/
    document.getElementsByTagName("h1")[0].innerHTML = data.title;
    document.getElementById("subtitle").innerHTML = data.subtitle;
    
    var displayOnload = "<a href=\""+ data.firstPlot +"\"><img src=\""+ data.firstPlot +"\"></a>";
    document.getElementById("left").getElementsByClassName("display_container")[0].innerHTML = displayOnload; 
    document.getElementById("right").getElementsByClassName("display_container")[0].innerHTML = displayOnload; 

    // List all radio buttons from left (0) and right (1) panels: 
    var leftRBs  = document.forms[0].getElementsByClassName('form-radio');
    var rightRBs = document.forms[1].getElementsByClassName('form-radio');  
    
    // Loop left and right radio buttons in // and attach click event handler:
    for (var j = [0]; j < leftRBs.length; j++) {
        leftRBs[j].onclick=radioClicked;
        rightRBs[j].onclick=radioClicked;
    }
}
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function loadPlotNotFound()
{
    alert('Image could not be loaded');
    this.src=data.plotNotFound;
    alert(this.src);
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function radioClicked() 
{
    
    // is the clicked radio button from left or right panel?
    var formName = this.parentNode.parentNode.parentNode.id;
    var formID = formName.startsWith("left") ? 0 : 1;        //alert(formID); 
    
    var plotFileName = "";    
     
    // Loop over tags in order to build the string plotFileName
    for (var i = [0]; i < data.tagArray.length; i++) {

        // Get all radio buttons from current tag:
        var RBs = document.forms[formID].elements[data.tagArray[i]];
        
        // What is the clicked value?
        for (var j = [0]; j < RBs.length ; j++) {
            if (RBs[j].checked){
                plotFileName += RBs[j].value;
            
                if (i < data.tagArray.length - 1) // not last
                    plotFileName += "__";
            }
        }
    } 
    //alert(plotFileName);
    var plotPath = data.plots_directory + "/" + plotFileName;
    
    // Does the plot exist?
    var innerHTML =  "<a href=\"" + plotPath + ".pdf\"><img src=\"" + plotPath+ ".png\" onerror=\"javascript:this.src='" + data.plotNotFound + "'\"></a>";  
    //alert(innerHTML);
    document.getElementById(formName).getElementsByClassName("display_container")[0].innerHTML = innerHTML;
}


