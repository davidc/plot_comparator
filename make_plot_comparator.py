#!/usr/bin/python3

# ##########################################################
# Script to create a web-based plot comparator
#
# Author: Claire David
#   Date: August 2019
#
# Using python3
#
# Takes a json file as argument
# 
# See a working example in ___________ 
# 
#
# ##########################################################

import os
import sys
import json
import glob
import subprocess
import shutil


# *************************************************

def usage():

    print("Usage:")
    print('python '+sys.argv[0]+' PATH_TO_JSON_CONFIG_FILE')
    print('Example:\npython '+sys.argv[0]+' ___________')
    sys.exit(2)

# *************************************************

def get_tag_container(tag, tagDict):

    htlm_div  = ""
    
    # Unactive tags won't be displayed but need to be check 
    # for the javascript to get the plot file name properly
    isActive  = True if tagDict["active"] == True else False
    display   = "" if isActive else " style=\"display:none;\""


    htlm_div += '    <div class="tag_container"'+ display +'>\n'
    htlm_div += '      <span class="tag_label">'+ tagDict["htmlLabel"] +'</span>\n'
    
    # Listing radio buttons (first one is checked by default)
    first   = True 
    checked = " checked"

    for buttonKey, buttonLabel in tagDict["values"].items():
        
        htlm_div += '      <input type="radio" name="'+ tag +'" class="form-radio" value="'+ buttonKey +'"'+ checked +'><label>'+ buttonLabel +'</label>\n'
        
        if first:
            checked = ""
            first   = False

        # Non-active tag: need to display a dummy radio button 
        # for the javascript to pick the non-display first one in a list
        if not isActive:
            htlm_div += '      <input type="radio" name="'+ tag +'" class="form-radio" value="dummy"><label>dummy</label>\n'
    
    htlm_div += '    </div>\n'

    return htlm_div

# *************************************************

def main():

    if len(sys.argv[1:]) < 1:
        usage()
    
    f_json     = sys.argv[1]
    print("\n Starting script " + sys.argv[0] + " ... \n Parsing json file: " + f_json)

    try:
        with open(f_json) as f:
            data = json.load(f)
    except ValueError as e:
        print("\nIssue while parsing json file.")
        sys.exit(1)

    outDir = data["output_directory"]

    print("\n============= C o n f i g u r a t i o n =============\n")

    print("Plot source directory        :  " + data["plots_directory"])
    print("Page title                   :  " + data["title"])
    print("Unactive tags                :  ", end=''); print(*[utag for utag, utagData in data["tags"].items() if utagData["active"] == False], sep = ', ' , end = '\n')
    print("Active tags                  :  ", end=''); print(*[atag for atag, atagData in data["tags"].items() if atagData["active"] == True ], sep = ', ' , end = '\n')
    print("Number of plots (png format) :  " + str(len(glob.glob(data["plots_directory"] + "/*.png" ))))
    print("Number of plots (pdf format) :  " + str(len(glob.glob(data["plots_directory"] + "/*.pdf" ))))
    print("Output directory (created)   :  " + outDir)
    
    #----- Creating output directory and copying files
    if not os.path.exists(outDir):
        os.makedirs(outDir)
        os.makedirs(outDir + "/css")
        os.makedirs(outDir + "/js")
        os.makedirs(outDir + "/img")

    shutil.copy2("css/style_plot_comparator.css", outDir + "/css")
    shutil.copy2("js/plot_comparator.js", outDir + "/js")
    shutil.copy2("img/ti_click.png", outDir + "/img")
    shutil.copy2(data["placeholder"], outDir + "/img")
    
    #----- Writing index.html
    print("\n ... Generating plot comparator index HTML ... \n")
    content = r'''<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" media="screen" type="text/css" title="Design" href="css/style_plot_comparator.css">
  <title>Plot comparator</title>
  <script type="text/javascript">
    window.data = {
'''
    content += '      title           : "' + data["title"] + '", \n'
    content += '      subtitle        : "' + data["subtitle"] + '", \n'
    content += '      plots_directory : "' + data["plots_directory"] + '", \n'
    content += '      firstPlot       : "./img/ti_click.png",\n'
    content += '      plotNotFound    : "' + data["placeholder"] + '",\n'
    strListTags = ""
    #for tag, tagData in data["tags"].items():
    #    strListTags += "\"" +  + "\", "
    listTags = [tag for tag, tagData in data["tags"].items()]
    content += '      tagArray        : ' + str(listTags) + '\n'
    content += r'''    };
  </script>
  <script type="text/javascript" src="js/plot_comparator.js"></script>
</head>
<body>
<div class="content">
<h1>Title</h1>
<p id="subtitle">Subtitle</p>
<form id="left">
  <div class="panel">
'''
    
    #----- Tags and radio buttons left panel:
    
    for tag, tagData in data["tags"].items():

        content += get_tag_container(tag, tagData)


    #----- Closing left panel, opening right panel:
    content += r'''
  </div>

  <div class="display_container">
  </div>
</form>
<form id="right">
  <div class="panel">
'''

    #----- Tags and radio buttons right panel:
    
    for tag, tagData in data["tags"].items():

        content += get_tag_container(tag, tagData)

    #----- Closing right panel and html document
    content += r'''
  </div>
  <div class="display_container">
  </div>
</form>
</div>
<footer class="footer">
    <p>Plot comparator available <a href="https://gitlab.cern.ch/davidc/plot_comparator">here</a>.</p>
</footer>
</body>
</html>'''

    #----- Saving html file
    f_html = outDir + "/index.html"
    with open(f_html,'w') as f:
        f.write(content)

    #----- Say bye
    print("HTML file created in         :  " + f_html)
    print("\n======================= b y e =======================\n")

# *************************************************

if __name__ == '__main__':

    main()

    
